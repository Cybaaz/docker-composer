import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/pages/Home';
import VeeValidate from 'vee-validate';

Vue.use(Router);
Vue.use(VeeValidate);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    }
  ]
});
